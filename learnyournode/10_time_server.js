// Problem # 10 - TIME SERVER

var server = require('./socket_server');
var port = process.argv[2];
var dt = new Date();
//"2013-07-06 07:42"
var year = dt.getFullYear();
var month = dt.getMonth() + 1;
month = (month < 10) ? String(0) + month : month;
var day = (dt.getDate() < 10) ? String(0) + dt.getDate() : dt.getDate();
var hours = (dt.getHours() < 10) ? String(0) + dt.getHours() : dt.getHours();
var minutes = dt.getMinutes();
minutes = (minutes < 10) ? String(0) + minutes : minutes;
var fmtDate = year + "-" + month + "-" + day + " " + hours + ":" + minutes + "\n";
server.start(fmtDate, process.argv[2]);
