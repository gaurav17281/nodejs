// Problem # 12 - HTTP UPPERCASERER

var http = require('http');
var map = require('through2-map');

var port = process.argv[2];

uc = map(function(chunk) {
  return chunk.toString().toUpperCase();
});

var onRequest = function(request, response) {
	if (request.method == 'POST') {
		request.pipe(uc).pipe(response);
	} else {
		response.writeHead(403);
		response.end();
	}
}

http.createServer(onRequest).listen(port);
console.log("Server has started. port:"+port);
