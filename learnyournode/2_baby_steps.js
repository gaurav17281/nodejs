// Problem # 2 - BABY STEPS

var sum = function(numbers) {
	var result = 0;
	if (numbers.length > 2) {
		for (i=2;i<numbers.length;i++) {
			result = Number(result) + Number(numbers[i]);
		}
	}
	return result;
}
var res = sum(process.argv);
console.log(res);
