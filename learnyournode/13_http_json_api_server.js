// Problem # 13 - HTTP JSON API SERVER

var http = require('http');
var url = require('url');

var port = process.argv[2];

///api/parsetime?iso=2013-08-10T12:10:15.474Z
///api/unixtime

var onRequest = function(request, response) {
	console.log("url=" + request.url);
	var parsedUrl = url.parse(request.url, true);
	console.log("End point = " + parsedUrl.pathname);
	var iso = parsedUrl.query.iso;
	var d = new Date(iso);
	var resp = null;
	if (parsedUrl.pathname.match("/api/parsetime")) {
		response.writeHead(200);
		var resp = {
      			hour: d.getHours(),
      			minute: d.getMinutes(),
      			second: d.getSeconds()
    		};
	} else if (parsedUrl.pathname.match("/api/unixtime")) {
		response.writeHead(200);
		var resp = {
			unixtime : d.getTime()
		};
	} else {
		response.writeHead(404);
	}
	if (resp) {
		response.write(JSON.stringify(resp));
	}
	response.end();
}


http.createServer(onRequest).listen(port);
console.log("Server has started. port:"+port);
