var net = require("net");

function start(msg, port) {
  function onRequest(socket) {
    console.log("Request received. " + msg);
    socket.write(msg);
    socket.end();
  }
  if (!port) {
	port = 8888;
  }
  net.createServer(onRequest).listen(port);
  console.log("Server has started. port:"+port);
}
exports.start = start;
