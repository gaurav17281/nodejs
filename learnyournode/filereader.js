var fs = require('fs');
var path = require('path');

module.exports = function readdir(dirpath, extn, callback) {
	var strPath = String(dirpath);
	fs.readdir(strPath, function(err, fileList) {
	if (err) {
		return callback(err);
	}
	var list = new Array();
        for (i=0;i<fileList.length;i++) {
                var ext = String(path.extname(fileList[i]));
                if (ext.match(extn)) {
			list[list.length] = fileList[i];
                        console.log(fileList[i]);
                }
        }
	callback(null, list);
	});
}
