var http = require("http");
var fs = require("fs");
var fileName = "";

function setFile(name) {
	fileName = name;	
}

function start(msg, port) {
  function onRequest(request, response) {
    console.log("Request received. msg=" + msg + "file=" + fileName);
    response.writeHead(200, {"Content-Type": "text/plain"});
    if (fileName) {
        var readStream = fs.createReadStream(fileName);
	readStream.pipe(response);
	readStream.on("end", function() {
	    response.end();
	});
    } else if (msg) {
        response.end(msg);
    } else {
        response.end("No Data to write");
    }
  }
  if (!port) {
	port = 8888;
  }
  http.createServer(onRequest).listen(port);
  console.log("Server has started. port:"+port);
}
exports.start = start;
exports.setFile = setFile;
