// Problem # 10 - HTML STREAM

var trumpet = require('trumpet');
var fs = require('fs');
var tr = trumpet();

tr.selectAll('.loud', function(element) {
	var stream = element.createStream();
	stream.on('data', function(d) {
		stream.write(d.toString().toUpperCase());
	});
});

process.stdin.pipe(tr).pipe(process.stdout);
