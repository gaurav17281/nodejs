// Problem # 5 - LINES

var through = require('through');
var split = require('split');
var lineNumber = 1;

var writeFunc = function(data) {
	this.queue((lineNumber % 2) ? data.toString().toLowerCase() : data.toString().toUpperCase());
	this.queue("\n");
	lineNumber++;
}

var endFunc = function() {
}

var throughStream = through(writeFunc, endFunc);
process.stdin.pipe(split()).pipe(throughStream).pipe(process.stdout);

