// Problem # 7 - HTTP SERVER

var http = require('http');
var through = require('through');

var write = function (buf) {
	var response = buf.toString().toUpperCase();
	this.queue(buf.toString().toUpperCase());
}

var end = function () { 
	this.queue(null); 
}

var callback = function(req, res) {
	if (req.method != 'POST') {
		res.writeHead(405);
		res.end();
	}
	req.pipe(through(write)).pipe(res);
}

http.createServer(callback).listen(process.argv[2]);
console.log('Started Server : ' + process.argv[2]);
