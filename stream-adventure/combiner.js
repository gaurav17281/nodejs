// Problem # 13 - COMBINER

var combine = require('stream-combiner');
var split = require('split');
var zlib = require('zlib');
var through = require('through');

module.exports = function() {
	var grouper = through(write, end);
	var current;

	function write(buff) {
		if (buff.length == 0)  return;
		var row = JSON.parse(buff);
		if (row.type == 'genre') {
			if (current) {
                		this.queue(JSON.stringify(current) + '\n');
            		}
            		current = { name: row.name, books: [] };
		} else {
			current.books.push(row.name);
		}
	}

	function end(){
		if (current) {
			this.queue(JSON.stringify(current) + "\n");
			this.queue(null);
		}
	}

	return combine(split(), grouper, zlib.createGzip());
}
