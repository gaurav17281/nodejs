// Problem # 4 - TRANSFORM

var through = require('through');

var writeFunc = function(data) {
	this.queue(data.toString().toUpperCase());
}

var endFunc = function() {
}

var throughStream = through(writeFunc, endFunc);
process.stdin.pipe(throughStream).pipe(process.stdout);

