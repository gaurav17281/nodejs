// Problem # 6 - Typed Arrays

process.stdin.on('data', function(d) {
	var array = new Uint8Array(d.length);
	for (i=0;i<d.length;i++) {
		array[i] = d[i];
	}
	console.log(JSON.stringify(array));
});
