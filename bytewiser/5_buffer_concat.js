// Problem # 5 - Buffer concat

var data = [];
process.stdin.on('data', function(d) {
	data.push(d);
});

process.stdin.on('end', function() {
	console.log(Buffer.concat(data));
});
