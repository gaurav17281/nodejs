// Problem # 5 - Basics : BATCH
var fs = require('fs')
var level = require('level')
var db = level(process.argv[2])

fs.readFile(process.argv[3], function (err, data) {
  if (err) throw err;
  var batch = db.batch();
  var lines = data.toString().split('\n');
  for (var line in lines) {
	console.log(line);
	var arr = line.split(',');
	if (arr[0] == 'put') {
		batch.put(arr[1], arr[2]);
	} else {
		batch.del(arr[1]);
	}
  }
  batch.write();
});
