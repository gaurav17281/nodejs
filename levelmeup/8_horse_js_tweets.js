// Problem # 8 - housr_js_tweets
var level = require('level')
// 2013-08-24T01:39:51.000Z=fat stacks of fat arrows
module.exports = function (db, date, callback) {
	var lastDate = null;
	var tweetArr = [];
	db.createReadStream({start : date, end : date + '\xff'})
		.on('data', function (data) {
			tweetArr.push(data.value);
  		})
		.on('error', function(err) {
			if (callback) {
				callback(err);
			}
		})
		.on('end', function() {
			if (callback) {
				callback(null, tweetArr);
			}
		})
  }
