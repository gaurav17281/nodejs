// Problem # 7 - horse_js_count
var level = require('level')
// 2013-08-24T01:39:51.000Z=fat stacks of fat arrows
module.exports = function (db, date, callback) {
	var lastDate = null;
	var count = 0;
	db.createReadStream({start : date})
		.on('data', function (data) {
			count++;
  		})
		.on('error', function(err) {
			if (callback) {
				callback(err);
			}
		})
		.on('end', function() {
			if (callback) {
				callback(null, count);
			}
		})
  }
