// Problem # 11 - Sublevel
var level = require('level')
var sub = require('level-sublevel')
var db = sub(level(process.argv[2]))

var db1 = db.sublevel('robot');
db1.put('slogan', 'beep boop', function(err) {
	if (err)
		console.log(err);
});

var db2 = db.sublevel('dinosaurs');
db2.put('slogan', 'rawr', function(err){
	if (err)
		console.log(err);
});
