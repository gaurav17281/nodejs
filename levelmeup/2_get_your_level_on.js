// Problem # 2 - Get Your Level On
var level = require('level')
var db = level(process.argv[2])
db.get('levelmeup', function(error, value) {
	if (error) {
		console.log(error);
		return;
	}
	console.log(value);
});

