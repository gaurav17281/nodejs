// Problem # 3 - Basics : GET
var level = require('level')
var db = level(process.argv[2])

function readData(keyIndex) {
	var key = 'gibberish' + keyIndex;
	db.get(key, function(error, value) {
		//console.log('key=' + key + ' error=' + error + ' value='+value); 
		if (error) {
			//console.log(error);
		} else {
			console.log(key + '=' + value)
		}
		if (keyIndex < 100) {
			readData(keyIndex + 1);
		}
	});
}
readData(0);
