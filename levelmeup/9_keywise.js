// Problem # 9 - Keywise
var level = require('level')
var db = level(process.argv[2], { valueEncoding: 'json' })
var jsonObj = require(process.argv[3])


var ops = jsonObj.map(function(record) {
	var key;
	if(record.type == 'user') {
		key = record.name;	
	} else if (record.type == 'repo') {
		key = record.user + "!" + record.name;	
	}
	return {type: 'put', key: key, value: record};
})

db.batch(ops);

// { "type": "user", "name": "maxogden" }
// { "type": "repo", "name": "mux-demux", "user": "dominictarr" }
